class Nodo:
    def __init__(self, dato):
        self.dato = dato
        self.siguiente = None

    def __str__(self):
        return str(self.dato)

    """
    def __str__(self):
        return f'soy un nodo con valor: {str(self.dato)}, mi direccion en ' \
        f'memoria es {super().__str__()}'
    """

    def agregar_siguiente(self, siguiente_nodo):
        self.siguiente = siguiente_nodo

    def get_siguiente(self):
        return self.siguiente
