from lista_orden import Lista

"""
nodo_1 = Nodo(54)
print('Nodo:', nodo_1)
print('Valor:', nodo_1.dato)
print('siguiente:', nodo_1.siguiente)

nodo_2 = Nodo(26)
print('Nodo:', nodo_1)
print('Valor:', nodo_1.dato)
print('siguiente:', nodo_1.siguiente)

nodo_1.agregar_siguiente(nodo_2)
print('despues de enlazar')
print(nodo_1.siguiente)
print(nodo_1.siguiente.dato, nodo_2.dato)
print(nodo_1.siguiente == nodo_2)
"""

lista = Lista()
lista.agregar_nodo(3)
lista.agregar_nodo(4)
lista.agregar_nodo(7)
lista.agregar_nodo(9)
lista.mostrar_lista()
print(lista.longitud_lista())
print(lista.buscar(3))
print(lista.eliminar(7))
lista.mostrar_lista()
print(lista.eliminar(9))
lista.mostrar_lista()
