from listas import Nodo


class Lista:
    inicio = None

    def agregar_nodo(self, item):
        nuevo = Nodo(item)
        nuevo.agregar_siguiente(self.inicio)
        self.inicio = nuevo

    def longitud_lista(self):
        contador = 0
        nodo_actual = self.inicio
        while nodo_actual is not None:
            contador += 1
            nodo_actual = nodo_actual.get_siguiente()
        return contador

    def mostrar_lista(self):
        nodos = ''
        nodo_actual = self.inicio
        while nodo_actual is not None:
            nodos += str(nodo_actual.dato)
            nodo_actual = nodo_actual.get_siguiente()
        print(nodos)

    def buscar(self, item):
        nodo_actual = self.inicio
        while nodo_actual is not None:
            if nodo_actual.dato == item:
                return True
            nodo_actual = nodo_actual.get_siguiente()
        return False

    def eliminar(self, valor):
        actual = self.inicio
        previo = None
        eliminado = None
        while actual:
            if actual.dato == valor:
                eliminado = actual
                if previo:
                    previo.siguiente = actual.siguiente
                else:
                    self.inicio = actual.siguiente
                return eliminado
            previo = actual
            actual = actual.siguiente
        return eliminado
