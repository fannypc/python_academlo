def fizzbuzz():
# ecuacion: 8n + 1
# big O: O(n)
    numeros=[]

    for i in range(1, 101): # n
        if i % 3 == 0 and i % 5 == 0: #1
            numeros.append('fizzbuzz') #1
            #print('fizzbuzz')
        elif i % 3 == 0: #1
            numeros.append('fizz') #1
            #print('fizz')
        elif i % 5 == 0: #1
            numeros.append('buzz') #1
            #print('buzz')
        else: #1
            numeros.append(i) #1
            #print(i)
    return numeros


print(fizzbuzz())
