from pilas import Pila


def binario(numero):
    pila = Pila()
    resultado = ''

    binariorecursivo(numero, pila)

    while not pila.esta_vacia():
        resultado = resultado + str(pila.extraer())
    return resultado


def binariorecursivo(numero, pila):
    if numero < 2:
        pila.incluir(numero)
        return numero
    else:
        residuo = numero % 2
        pila.incluir(residuo)
        numero = numero // 2
        binariorecursivo(numero, pila)


if __name__ == '__main__':
    resultado = binario(8)
    print(resultado)
